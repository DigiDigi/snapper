# snapper
A script for quick screenshot snippets with immediate display and clean resizability.

Can also open images directly: `python3 snapper.py image.png`

- Depends on qt (PySide2)
- snapper.py uses gnome-screenshot
- snapper_kde.py uses spectacle
____

![example image](/images/example1.png)
